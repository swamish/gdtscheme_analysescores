
<h1>Table of Contents<span class="tocSkip"></span></h1>
<div class="toc"><ul class="toc-item"><li><span><a href="#Why-did-we-try-a-GDT-like-scoring-scheme?" data-toc-modified-id="Why-did-we-try-a-GDT-like-scoring-scheme?-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Why did we try a GDT like scoring scheme?</a></span></li><li><span><a href="#Read-in-the-scores" data-toc-modified-id="Read-in-the-scores-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Read in the scores</a></span></li><li><span><a href="#Read-obsolete-to-successor-mapping" data-toc-modified-id="Read-obsolete-to-successor-mapping-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Read obsolete to successor mapping</a></span></li><li><span><a href="#Parse-scores-of-stars-with-intact-composition" data-toc-modified-id="Parse-scores-of-stars-with-intact-composition-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Parse scores of stars with intact composition</a></span></li><li><span><a href="#Analyse-the-scores" data-toc-modified-id="Analyse-the-scores-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Analyse the scores</a></span></li></ul></div>

# Analyse Star Scores And Compare


```bash
%%bash
pwd
ls ..
```

    /home/zpoc/freyr/obsoletepdbs_work/gdtscheme/analysescores/bin
    analysisdatafiles
    bin
    outfiles
    predecessors
    successors



```bash
%%bash
# this is how a typical .cliquesores file in the gdt scoring scheme looks like
tail ../predecessors/1c75_from_1b7c_8_20_5_10_2000.cliquescores
```

    SCORE Superstar: 0.554
    ==> cliquescores/97_1c75_from_1b7c_r1_r1_r1_r1_r2_r2_r8_r9.clique <==
    SCORE Query: 0.73
    SCORE Superstar: 0.727
    ==> cliquescores/98_1c75_from_1b7c_r8_r1_r1_r1_r2_r2_r2_r9.clique <==
    SCORE Query: 0.801
    SCORE Superstar: 0.816
    ==> cliquescores/99_1c75_from_1b7c_r1_r1_r1_r1_r14_r2_r2_r9.clique <==
    SCORE Query: 0
    SCORE Superstar: 0


## Why did we try a GDT like scoring scheme?
+ The idea is that for a bad query star, there won't be stars in the PDB that are close to it in terms of both composition and geometry, so the score of the Query will be affected.
+ In case of stable stars, however, we expect 
  + the best-match (least RMSD of superimposition, with the same composition) star, otherwise called the _Superstar_ to be highly similar, ie. very low RMSD, usually seen to be less than 1Å.
  + similarity with the top 100 (_ad hoc_) stars, or all stars with less than _gdt-rmsd-cutoff_ RMSD of superimposition (taken to be 2Å, _ad hoc_), which ever is smaller. We may refer to this set as the _sisterly set_. Why should there be any similarity?
    + Consider any one set of stars in the PDB, all with the same composition. They will have different geometries, but they can be clustered into some clusters, such that within each cluster the stars don't vary too much in terms of their geometries. Now, we can't cluster because that will involve all-against-all superimposition of the stars and then finding out linkage distance in RMSD space, then clustering. Instead, consider the _sisterly set_. Even if they are not strictly part of the cluster that could have been formed had we done a proper (presumably hierarchical)  clustering, most of these stars should be from the same cluster, because they are of a similar geometry - one that matches to that of the query star. If the query star doesn't have similar stars in the PDB, most of these stars in the sisterly set won't be similar to each other.
    + As a result, if most stars are similar to the query star, and also to the Superstar, the score will tend towards 0 for both of them. We assume Superstar to be closer to the sisterly set, since we are assuming already that most of the stars in the PDB are stable, and Superstar is one of them.
    + On the other hand, if the query star is an unstable one, we expect it to be dissimilar to the sisterly set, and the score should tend towards 1, more so than the Superstar's score (since the Superstar will have at least some stars in the sisterly set that are similar to it)

## Read in the scores

For the sake of simplicity, create a symlink to the directory where cliq-mapping is present, for later use


```bash
%%bash
ln -sf ../../pdbmapping/cliqmapping/cliqmaps cliqmaps
ln -sf ../../pdbmapping/ pdbmapping
```


```python
from os import listdir
from os.path import isfile, join
preddir = "../predecessors/"
succdir = "../successors/"

# get filenames of all files in predecessors/ and successors/
predscorefiles = [f for f in listdir(preddir) if isfile(join(preddir, f))];
succscorefiles = [f for f in listdir(succdir) if isfile(join(succdir, f))];
```


```python
predscorefiles[:5]
```




    ['3ged_from_3dii_8_20_5_10_2000.cliquescores',
     '4qma_from_2gm6_8_20_5_10_2000.cliquescores',
     '4iqn_from_4hg1_8_20_5_10_2000.cliquescores',
     '3n10_from_3ghx_8_20_5_10_2000.cliquescores',
     '3w7t_from_3c68_8_20_5_10_2000.cliquescores']




```python
succscorefiles[:5]
```




    ['2wya_mod_8_20_5_10_2000.cliquescores',
     '2y4r_mod_8_20_5_10_2000.cliquescores',
     '2w6k_mod_8_20_5_10_2000.cliquescores',
     '4qyt_mod_8_20_5_10_2000.cliquescores',
     '3ohr_mod_8_20_5_10_2000.cliquescores']




```python
%%time
succpreddict = {}
succscoresdict = {}
predscoresdict = {}
# this dictionary shall hold records of the form
    # (succID, predID):(succscorefilepath, predscorefilepath)
for smobsfile in predscorefiles:
    try:
        succ = smobsfile.split("_")[0]
        pred = smobsfile.split("_")[2]
        for smsucc in succscorefiles:
            if smsucc.startswith(succ):
                smsuccfile = smsucc
        succpreddict[(succ, pred)]=(smsuccfile, smobsfile)
        #print succpreddict
    except IndexError as IE:
        print(smobsfile)
succpreddict;
```

    CPU times: user 3.19 ms, sys: 0 ns, total: 3.19 ms
    Wall time: 3.17 ms



```python
%%time
for smpair in sorted(succpreddict.keys()):
    succfile = succpreddict[smpair][0]
    succID = succfile.split("_")[0]
    predfile = succpreddict[smpair][1]
    predID = predfile.split("_")[2]
    with open(succdir+succfile, 'r') as succfobj:
        succf = succfobj.readlines()
    for i,succfline in enumerate(succf):
        if succfline.startswith("==>"):
            succcliqscorefilename = succfline.split()[1].strip('cliquescores/').split("_")
            cliqno = int(succcliqscorefilename[0])
            if succf[i+1].startswith("SCORE Query"):
                queryscore = float(succf[i+1].split(":")[1].strip())
                if succf[i+2].startswith("SCORE Superstar"):
                    superstarscore = float(succf[i+2].split(":")[1].strip())
                else:
                    superstarscore = 0
            # print(cliqno, succID, queryscore, superstarscore)
            succscoresdict.setdefault(succID, dict())[cliqno] = (queryscore, superstarscore)


    with open(preddir+predfile, 'r') as predfobj:
        predf = predfobj.readlines()
    for i,predfline in enumerate(predf):
        if predfline.startswith("==>"):
            predcliqscorefilename = predfline.split()[1].strip('cliquescores/').split("_")
            cliqno = int(predcliqscorefilename[0])
            if predf[i+1].startswith("SCORE Query"):
                queryscore = float(predf[i+1].split(":")[1].strip())
                if i+2<len(succf):
                    if predf[i+2].startswith("SCORE Superstar") and i+2<len(succf):
                        superstarscore = float(predf[i+2].split(":")[1].strip())
                    else:
                        superstarscore = 0
            # print(cliqno, succID, queryscore, superstarscore)
            predscoresdict.setdefault(predID, dict())[cliqno] = (queryscore, superstarscore)
        # print(predscoresdict)
```

    CPU times: user 2.23 s, sys: 42.8 ms, total: 2.27 s
    Wall time: 2.28 s


## Read obsolete to successor mapping


```python
with open("pdbmapping/obsolete_pc30_res1.8_R0.25_d180619_entries7336.txt") as obsdictobj:
    obsdictf = obsdictobj.readlines()
    obsdict = { k.split()[0].lower():k.split()[1].lower() for k in obsdictf }
```


```bash
%%bash
mkdir -p ../outfiles
```


```python
%%time
# loop over all predecessor pdbs
cgCentreMap = {}
for predID in sorted(obsdict.keys()):
    succID = obsdict[predID]
    with open("cliqmaps/"+obsdict[predID]+"_mod_from_"+predID+"_mod.cliqmap") as cliqmapobj:
        cliqmapf=cliqmapobj.readlines()
        for i, j in enumerate(cliqmapf):
            if j.startswith("CLIQUE"):
                predcentre = int(cliqmapf[i-2].split()[1])
                succcentre = int(cliqmapf[i-1].split()[1])
                # print(predcentre, succcentre)
                cgCentreMap.setdefault(predID, dict())[predcentre+1]=(succcentre+1, cliqmapf[i-1])
```

    CPU times: user 794 ms, sys: 38.8 ms, total: 833 ms
    Wall time: 863 ms


## Parse scores of stars with intact composition


```python
def findcompostring(cliqline, starsdir):
    thefileID = cliqline.split()[0]
    with open(starsdir+"/"+thefileID+".gpdb") as thegpdbfobj:
        thegpdbf = thegpdbfobj.readlines()
    cliqmembers = cliqline.strip().split()[1:]
    compolist = [thegpdbf[int(i)][12:16].strip() for i in cliqmembers]
    compostring = compolist[0] + "_" + "_".join(sorted(compolist[1:]))
    return compostring
```


```python
%%time
succIDlist = list(set([i[0] for i in succpreddict.keys()]))
succstarsdict = {}
for succID in sorted(succIDlist):
    with open('pdbmapping/cliqmapping/succstars/' + succID + '_mod.cliq') as smfobj:
        succf = smfobj.readlines()
    for i in succf:
        if i.startswith(succID):
            cliqno = int(i.strip().split()[1])+1
            #find compo here
            compostring = findcompostring(i, 'pdbmapping/cliqmapping/succgpdbs/')
            succstarsdict.setdefault(succID, dict())[cliqno] = (i, compostring)

```

    CPU times: user 45.4 s, sys: 6.04 s, total: 51.5 s
    Wall time: 52.5 s



```python
%%time
# for each predecessor
    # for each cliqmap in cliqmap file for this pair of pred:succ
        # if successor star has stayed intact in terms of composition
            # write out the scores for the cliqmap
outdir = '../analysisdatafiles/'
!mkdir -p '../analysisdatafiles'
for somepred in sorted(cgCentreMap.keys()):
    succstarID = obsdict[somepred]
    with open(outdir + succstarID + "_from_" + somepred + ".analysisdata.tsv", 'w') as theoutobj:
        theoutobj.write("pred\tsucc\tp-cen\ts-cen\tp_q-score\tp_s-score\ts_q-score\ts_s-score\n")
        nonintact = 0
        total = 0
        for cliqmap_predcentre in sorted(cgCentreMap[somepred].keys()):
            succstarcentre = cgCentreMap[somepred][cliqmap_predcentre][0]
            compo_shouldbe = findcompostring(cgCentreMap[somepred][cliqmap_predcentre][1], "pdbmapping/cliqmapping/succgpdbs/")
            total = total+1
            compois = succstarsdict[succstarID][succstarcentre][1]
            if compois == compo_shouldbe:
                predscores = predscoresdict[somepred][cliqmap_predcentre]
                succscores = succscoresdict[succstarID][succstarcentre]
                theoutobj.write("\t".join([somepred, succstarID, str(cliqmap_predcentre),
                                          str(succstarcentre), str(round(predscores[0], 3)),
                                           str(round(predscores[1], 3)), str(round(succscores[0], 3)),
                                                str(round(succscores[1], 3))]) + "\n")
```

    CPU times: user 46.7 s, sys: 9.8 s, total: 56.5 s
    Wall time: 57.4 s



```bash
%%bash
cat ../analysisdatafiles/* | grep -v "pred\|succ\|p-cen"| sed '1s/^/pred\tsucc\tp-cen\ts-cen\tp_q-score\tp_s-score\ts_q-score\ts_s-score\n/' > ../analysisdataall.tsv
```

## Analyse the scores


```python
import pandas as pd
from IPython.display import display
with open('../analysisdataall.tsv', 'r') as analobj:
    analf = analobj.readlines()

analf = [analf[0].split()]+[i.split()[:2]+list(map(int, i.split()[2:4]))+list(map(lambda x:float(x), i.split()[4:8])) for i in analf[1:]]
totalstars = len(analf)
analf_df = pd.DataFrame.from_records(analf)
display(analf_df[:4])
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>pred</td>
      <td>succ</td>
      <td>p-cen</td>
      <td>s-cen</td>
      <td>p_q-score</td>
      <td>p_s-score</td>
      <td>s_q-score</td>
      <td>s_s-score</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1b7c</td>
      <td>1c75</td>
      <td>1</td>
      <td>1</td>
      <td>0.619</td>
      <td>0.616</td>
      <td>0.619</td>
      <td>0.616</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1b7c</td>
      <td>1c75</td>
      <td>2</td>
      <td>2</td>
      <td>0.731</td>
      <td>0.684</td>
      <td>0.725</td>
      <td>0.677</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1b7c</td>
      <td>1c75</td>
      <td>3</td>
      <td>3</td>
      <td>0.728</td>
      <td>0.799</td>
      <td>0.778</td>
      <td>0.777</td>
    </tr>
  </tbody>
</table>
</div>



```python
# columns are:
# [ pred succ p-cen s-cen p_q-score p_s-score s_q-score s_s-score ]

# no of stars in pred that are bad at tolerance 0%
badstarspred10 = len([i for i in analf[1:] if i[4]>i[5]])
badstarssucc10 = len([i for i in analf[1:] if i[6]>i[7]])
print(badstarspred10, totalstars, badstarspred10/totalstars)
print(badstarssucc10, totalstars, badstarssucc10/totalstars)
predvssucc = len([i for i in analf[1:] if i[4]>i[6]])
print(predvssucc, totalstars, predvssucc/totalstars)
predvssucc = len([i for i in analf[1:] if i[4]<i[6]])
print(predvssucc, totalstars, predvssucc/totalstars)
predvssucc = len([i for i in analf[1:] if i[4]==i[6]])
print(predvssucc, totalstars, predvssucc/totalstars)
```

    38990 125861 0.30978619270465035
    38886 125861 0.3089598843168257
    48598 125861 0.38612437530291355
    47899 125861 0.38057062950397663
    29363 125861 0.23329704992015002



```python
with open('../analysisdatafiles/1nth_from_1l2r.analysisdata.tsv', 'r') as smfobj:
    smf = smfobj.readlines()
    smf = [i.split() for i in smf[1:]]
totpredscore = sum([float(i[4]) for i in smf])
totalstarsforthis = len(smf)
print(totpredscore, totalstarsforthis, totpredscore/totalstarsforthis)
totsuccscore = sum([float(i[6]) for i in smf])
print(totsuccscore, totalstarsforthis, totsuccscore/totalstarsforthis)
```

    166.52699999999993 517 0.3221025145067697
    165.61800000000008 517 0.3203442940038686

