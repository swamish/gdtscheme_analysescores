#!/usr/bin/env bash

for i in /storage/swastik/workingdir/obsoletepdbrefinement/scoringnr30pdbs_20180805/*/??????????????.gpdb;do 
    basegpdb=$(basename $i .gpdb)
    dirnamegpdb=$(dirname $(realpath $i))
    pdbID=${basegpdb:10:15}
    echo $i
    cp $i modpredecgpdbs/$pdbID"_mod.gpdb"
done

for i in /storage/swastik/workingdir/obsoletepdbrefinement/scoringnr30pdbs_20180805/*/??????????????.cliq;do 
    basegpdb=$(basename $i .cliq)
    dirnamegpdb=$(dirname $(realpath $i))
    pdbID=${basegpdb:10:15}
    echo $i
    cp $i modpredecstars/$pdbID"_mod.cliq"
done
#for i in /storage/swastik/workingdir/obsoletepdbrefinement/scoringnr30pdbs_20180805/*/??????????????.gpdb;do echo $i; cp $i modpredecgpdbs/ ;done
#for i in /storage/swastik/workingdir/obsoletepdbrefinement/scoringnr30pdbs_20180805/*/??????????????.cliq;do echo $i; cp $i modpredecstars/ ;done
